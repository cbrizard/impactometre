'use strict'

const express = require('express')

const meetingScenarios = require('../../../database/meeting/meetingScenarios')
const { normaliseDamages } = require('../../../utils/normalise')
const { exportResults } = require('../../services')

const router = express.Router()

router.get('/:userId', function (req, res, next) {
  const userId = req.params.userId

  const scenarios = []
  meetingScenarios.forEach(scenario => {
    if (scenario.user === userId) {
      scenarios.push(scenario)
    }
  })

  // Export reference scenario results to JSON file
  const referenceScenario = scenarios.find(s => s.name === 'Référence')
  exportResults.run(referenceScenario)

  const normalisedDamages = normaliseDamages(scenarios)

  res.render('meeting/results/results', { title: 'Résultats', scenarios, normalisedDamages })
})

module.exports = router
