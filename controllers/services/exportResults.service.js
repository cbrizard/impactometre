const _ = require('lodash')
const fs = require('fs')

/**
 * Function used to JSON stringify an object
 * with nested maps.
 * @param {string} key - The key of the object
 * @param {any} value - The value of the key
 */
function replacer (key, value) {
  if (value instanceof Map) {
    const obj = {}
    for (const [k, v] of value) {
      obj[k] = v
    }

    return obj
  }

  return value
}

function run (referenceScenario) {
  const exportPath = 'database/exports/results.json'

  // Copy by value so as not to modify the original data
  const copy = _.cloneDeep(referenceScenario)

  const toExport = JSON.stringify(copy, replacer, 2)

  fs.writeFile(exportPath, toExport, (err) => {
    if (err) throw err
    console.log('Results export success')
  })
}

module.exports = {
  run
}
